
const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');

const app = express();

const cursoRouter = require('./routes/cursoRoutes');
const alumnoRouter = require('./routes/alumnoRoutes');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger('dev')); //muestra en consola las peticiones Get, Post... recibidas

//definicion de rutas
cursoRouter(app);
alumnoRouter(app);

// conexión mongoose a mongodb
mongoose.Promise = global.Promise;
let mongodbURI = 'mongodb://localhost:27017/academy';
mongoose.connect(mongodbURI, {useNewUrlParser: true}); 

const port = 5000
app.listen(port, () => console.log(`App en puerto ${port}!`))



/*
https://stackoverflow.com/questions/12495891/what-is-the-v-field-in-mongoose
*/
