const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';

MongoClient.connect(url, { useNewUrlParser: true })
.then((client) => {

    console.log('Conectado al servidor');

    const db = client.db(dbname);
    const collection = db.collection("cursos");

    collection.insertOne({"nombre": "Javascript NodeJS", "descripcion": "curso javascript 150h"})
    .then((result) => {
        console.log("Despues de insertar:\n");
        console.log(result.ops);
        return collection.find({}).toArray();
    })
    .then((docs)=>{
        console.log("Encontrado:\n");
        console.log(docs);
        return db.dropCollection("cursos");
    })
    .then((result)=>{
        console.log("Tancat!");
        client.close();
    })
    .catch((err) => console.log(err));

}).catch((err) => console.log(err));

  