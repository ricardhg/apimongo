'use strict';



const Alumno = require('../models/alumnoModel');



exports.list_all_alumnos = function(req, res) {
  Alumno.find({}, function(err, alumno) {
    if (err)
      res.send(err);
    res.json(alumno);
  });
};


exports.create_a_alumno = function(req, res) {
  var new_alumno = new Alumno(req.body);
  new_alumno.save(function(err, alumno) {
    if (err)
      res.send(err);
    res.json(alumno);
  });
};


exports.read_a_alumno = function(req, res) {
  Alumno.findById(req.params.alumnoId, function(err, alumno) {
    if (err)
      res.send(err);
    res.json(alumno);
  });
};

exports.update_a_alumno = function(req, res) {
  // console.log("REBUT", req.body);
  Alumno.findOneAndUpdate({_id: req.params.alumnoId}, req.body, {new: true}, function(err, alumno) {
    if (err)
      res.send(err);
    res.json(alumno);
  });
};



exports.delete_a_alumno = function(req, res) {
  Alumno.remove({
    _id: req.params.alumnoId
  }, function(err, alumno) {
    if (err)
      res.send(err);
    res.json({ message: 'Alumno borrado' });
  });
};
