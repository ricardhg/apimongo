'use strict';



const Curso = require('../models/cursoModel');



exports.list_all_cursos = function(req, res) {
  Curso.find({}, function(err, curso) {
    if (err)
      res.send(err);
    res.json(curso);
  });
};


exports.create_a_curso = function(req, res) {
  var new_curso = new Curso(req.body);
  new_curso.save(function(err, curso) {
    if (err)
      res.send(err);
    res.json(curso);
  });
};


exports.read_a_curso = function(req, res) {
  Curso.findById(req.params.cursoId, function(err, curso) {
    if (err)
      res.send(err);
    res.json(curso);
  });
};

exports.update_a_curso = function(req, res) {
  // console.log("REBUT", req.body);
  Curso.findOneAndUpdate({_id: req.params.cursoId}, req.body, {new: true}, function(err, curso) {
    if (err)
      res.send(err);
    res.json(curso);
  });
};


exports.delete_a_curso = function(req, res) {
  Curso.remove({
    _id: req.params.cursoId
  }, function(err, curso) {
    if (err)
      res.send(err);
    res.json({ message: 'Curso borrado' });
  });
};
