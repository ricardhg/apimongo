module.exports = function (app) {

    const alumno = require('../controllers/alumnoController');


    // alumno Routes
    app.route('/alumnos')
        .all((req, res, next) => {
            res.statusCode = 200;
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.setHeader('Content-Type', 'application/json'); // RETURNING JSON
            next();
        })
        .get(alumno.list_all_alumnos)
        .post(alumno.create_a_alumno)
        .options((req, res, next) => {
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
            res.end();
        });


    app.route('/alumnos/:alumnoId')
        .all((req, res, next) => {
            res.statusCode = 200;
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.setHeader('Content-Type', 'application/json'); // RETURNING JSON
            next();
        })
        .get(alumno.read_a_alumno)
        .put(alumno.update_a_alumno)
        .delete(alumno.delete_a_alumno)
        .options((req, res, next) => {
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
            res.end();
        });

};