module.exports = function (app) {

    const curso = require('../controllers/cursoController');


    // curso Routes
    app.route('/cursos')
        .all((req, res, next) => {
            res.statusCode = 200;
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.setHeader('Content-Type', 'application/json'); // RETURNING JSON
            next();
        })
        .get(curso.list_all_cursos)
        .post(curso.create_a_curso)
        .options((req, res, next) => {
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
            res.end();
        });


    app.route('/cursos/:cursoId')
        .all((req, res, next) => {
            res.statusCode = 200;
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.setHeader('Content-Type', 'application/json'); // RETURNING JSON
            next();
        })
        .get(curso.read_a_curso)
        .put(curso.update_a_curso)
        .delete(curso.delete_a_curso)
        .options((req, res, next) => {
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
            res.end();
        });

};