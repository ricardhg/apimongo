

//https://mongoosejs.com/docs/guide.html

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CursoSchema = new Schema({
  nombre: {
    type: String,
    required: 'Nombre del curso'
  },
  especialidad: {
    type: String
  },
});

module.exports = mongoose.model('Curso', CursoSchema);