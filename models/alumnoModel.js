

//https://mongoosejs.com/docs/guide.html

'use strict';

// const Curso = require('../models/cursoModel');


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AlumnoSchema = new Schema({
  nombre: {
    type: String,
    required: 'Nombre del alumno'
  },
  email: {
    type: String
  },
  genero: {
    type: String
  },
  edad: {
    type: Number
  },
  cursos:[{ type : Schema.Types.ObjectId, ref: 'Curso' }]
  
});

module.exports = mongoose.model('Alumno', AlumnoSchema);